#!/usr/bin/env bash
# setup.sh 8.9.4

nodeVersion=$1

echo "Node Version is $nodeVersion"

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

echo "Nvm Done"

nvm install $nodeVersion

nvm use $nodeVersion

nvm alias default $nodeVersion

echo "Npm Done"

npm install -g @angular/cli

npm install

echo "Package Done"

