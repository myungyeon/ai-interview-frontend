import { Auth } from "aws-amplify";

const environment = {
  name: "environment.dev",
  amplify: {
    auth: {
      // REQUIRED - Amazon Cognito Identity Pool ID
      identityPoolId: "ap-southeast-1:357c7958-85ec-46e9-9827-4c7fe5cc542a",
      // REQUIRED - Amazon Cognito Region
      region: "ap-southeast-1",
      // OPTIONAL - Amazon Cognito User Pool ID
      userPoolId: "ap-southeast-1_jNYI9xVlS",
      // OPTIONAL - Amazon Cognito Web Client ID
      userPoolWebClientId: "4o91in7al3tcums0taai3t1bi4"
    },
    sns: {
      region: "ap-southeast-1",
      identityPoolId: "ap-southeast-1:357c7958-85ec-46e9-9827-4c7fe5cc542a"
    },
    api: {
      endpoints: [
        {
          name: "AI-Interview-Auth",
          endpoint:
            "https://gacju54q0c.execute-api.ap-southeast-1.amazonaws.com/dev",
          custom_header: async () => {
            // return { Authorization : 'token' }
            // Alternatively, with Cognito User Pools use this:
            // const { idToken } = await Auth.currentSession();
            // return { Authorization: idToken.jwtToken };
            return {
              Authorization: (await Auth.currentSession())
                .getIdToken()
                .getJwtToken()
            };
          }
        },
        {
          name: "AI-Interview-UnAuth",
          endpoint:
            "https://gacju54q0c.execute-api.ap-southeast-1.amazonaws.com/dev"
        }
      ]
    }
  }
};

export default environment;
