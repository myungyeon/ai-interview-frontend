const environment = require(`./environment.${process.env.VUE_APP_MODE}.js`)
  .default;
export default environment;
