import { Auth } from "aws-amplify";

const environment = {
  name: "environment.prod",
  amplify: {
    auth: {
      // REQUIRED - Amazon Cognito Identity Pool ID
      identityPoolId: "ap-southeast-1:e76c6fc9-bab6-4c4a-9293-f147c4a81b59",
      // REQUIRED - Amazon Cognito Region
      region: "ap-southeast-1",
      // OPTIONAL - Amazon Cognito User Pool ID
      userPoolId: "ap-southeast-1_TdaffZf0Q",
      // OPTIONAL - Amazon Cognito Web Client ID
      userPoolWebClientId: "47iqprcuh6n63b19058diavlt0"
    },
    sns: {
      region: "ap-southeast-1",
      identityPoolId: "ap-southeast-1:357c7958-85ec-46e9-9827-4c7fe5cc542a"
    },
    api: {
      endpoints: [
        {
          name: "AI-Interview-Auth",
          endpoint:
            "https://c9o1r13q49.execute-api.ap-southeast-1.amazonaws.com/prod",
          custom_header: async () => {
            // return { Authorization : 'token' }
            // Alternatively, with Cognito User Pools use this:
            // const { idToken } = await Auth.currentSession();
            // return { Authorization: idToken.jwtToken };
            return {
              Authorization: (await Auth.currentSession())
                .getIdToken()
                .getJwtToken()
            };
          }
        },
        {
          name: "AI-Interview-UnAuth",
          endpoint:
            "https://c9o1r13q49.execute-api.ap-southeast-1.amazonaws.com/prod"
        }
      ]
    }
  }
};

export default environment;
