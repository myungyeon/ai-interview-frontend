import Vue from "vue";

import App from "./App.vue";
import store from "./store";
import router from "./router";
import i18n from "./utils/i18n";
import Amplify from "aws-amplify";
import environment from "./environments";

import "./utils/analytics";
import "./utils/filters";
import "./utils/prototype";
import "./plugins/vuetify";
import "./styles.scss";
import "prismjs";
import "prismjs/components/prism-python";
import "sortablejs";
import "vuedraggable";

const AWS = require("aws-sdk");

Amplify.configure({
  Auth: environment.amplify.auth,
  API: environment.amplify.api,
  SNS: environment.amplify.sns
});

AWS.config.region = environment.amplify.sns.region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: environment.amplify.sns.identityPoolId
});

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount("#app");
