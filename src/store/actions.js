import { Auth, Storage, API } from "aws-amplify";
import environment from "@/environments";

const AWS = require("aws-sdk");

const apiName = "AI-Interview-Auth";
const apiUnAuthName = "AI-Interview-UnAuth";
const initData = {
  headers: {}
};

const setCandidateFullName = (
  value,
  firstName = "first_name",
  lastName = "last_name",
  fullName = "full_name"
) => {
  value[fullName] = `${value[firstName]}, ${value[lastName]}`;
  return value;
};

const setSessionCandidateFullName = value =>
  setCandidateFullName(
    value,
    "candidate_first_name",
    "candidate_last_name",
    "candidate_full_name"
  );

const handleError = (err, dispatch) => {
  const message =
    err.response && err.response.data ? err.response.data.Message : err.message;

  dispatch("openSnackbar", {
    value: true,
    text: message,
    timeout: 1000 * 3.5,
    color: "error"
  });
  console.error(err.response && err.response.data ? err.response.data : err);
  return err;
};

const actions = {
  async openSnackbar({ commit, dispatch }, payload) {
    try {
      await commit("OPEN_SNACKBAR", payload);
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async closeSnackbar({ commit, dispatch }) {
    try {
      await commit("CLOSE_SNACKBAR");
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** video ***/
  async uploadVideo({ commit, dispatch }, payload) {
    const { file, fileName } = payload;

    try {
      await dispatch("openSnackbar", {
        value: true,
        text: "Please wait a moment.\n" + "Uploading video",
        timeout: 1000 * 60 * 60 * 24,
        loading: true,
        multiLine: true
      });
      await commit("UPLOAD_VIDEO", {
        file: await Storage.put(fileName, file, {
          contentType: "video/webm;codecs=vp9",
          level: "public"
        }),
        fileName: fileName
      });
      await dispatch("closeSnackbar");
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** question ***/
  async getQuestions({ commit, dispatch }) {
    try {
      const response = await API.get(apiName, "/question", initData);
      commit("GET_QUESTIONS", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async addQuestion({ commit, dispatch }, payload) {
    try {
      const response = await API.post(apiName, "/question", {
        ...initData,
        body: payload
      });
      commit("ADD_QUESTION", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async editQuestion({ commit, dispatch }, { name, ...payload }) {
    try {
      const response = await API.put(apiName, `/question/${name}`, {
        ...initData,
        body: payload
      });
      commit("EDIT_QUESTION", {
        name,
        question: response
      });
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async deleteQuestion({ commit, dispatch }, questionName) {
    console.log(questionName);
    try {
      const response = await API.del(
        apiName,
        `/question/${questionName}`,
        initData
      );

      commit("DELETE_QUESTION", questionName);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** interview ***/
  async getInterviews({ commit, dispatch }) {
    try {
      const initData = {
        headers: {}
      };
      const response = await API.get(apiName, "/interview", initData);
      commit("GET_INTERVIEWS", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async getInterview({ commit, dispatch }, interviewName) {
    try {
      const response = await API.get(
        apiName,
        `/interview/${interviewName}`,
        initData
      );
      commit("GET_INTERVIEW", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async addInterview({ commit, dispatch }, payload) {
    try {
      const response = await API.post(apiName, "/interview", {
        ...initData,
        body: payload
      });
      commit("ADD_INTERVIEW", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async editInterview({ commit, dispatch }, { name, ...payload }) {
    console.log(name, payload);
    try {
      const response = await API.put(apiName, `/interview/${name}`, {
        ...initData,
        body: payload
      });
      commit("EDIT_INTERVIEW", {
        name,
        interview: response
      });
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async deleteInterview({ commit, dispatch }, interviewName) {
    try {
      const response = await API.del(
        apiName,
        `/interview/${interviewName}`,
        initData
      );

      commit("DELETE_INTERVIEW", interviewName);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async sendInvitations({ commit, dispatch }, { interview, candidates }) {
    try {
      const response = await API.post(
        apiName,
        `/interview/${interview}/invite`,
        {
          ...initData,
          body: {
            candidates: candidates
          }
        }
      );
      commit("SEND_INVITATIONS", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** candidate ***/
  async getCandidates({ commit, dispatch }) {
    try {
      const response = await API.get(apiName, "/candidate", initData);
      commit("GET_CANDIDATES", response.map(c => setCandidateFullName(c)));
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async getCandidate({ commit, dispatch }, candidateName) {
    try {
      const response = await API.get(
        apiName,
        `/candidate/${candidateName}`,
        initData
      );
      commit("GET_CANDIDATE", setCandidateFullName(response));
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async addCandidate({ commit, dispatch }, payload) {
    try {
      const response = await API.post(apiName, "/candidate", {
        ...initData,
        body: payload
      });
      commit("ADD_CANDIDATE", setCandidateFullName(response));
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async editCandidate({ commit, dispatch }, { name, ...payload }) {
    try {
      const response = await API.put(apiName, `/candidate/${name}`, {
        ...initData,
        body: payload
      });
      commit("EDIT_CANDIDATE", {
        name,
        candidate: setCandidateFullName(response)
      });
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async deleteCandidate({ commit, dispatch }, candidateName) {
    try {
      const response = await API.del(
        apiName,
        `/candidate/${candidateName}`,
        initData
      );

      commit("DELETE_CANDIDATE", candidateName);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** session ***/
  async getSession({ commit, dispatch }, sessionName) {
    try {
      const response = await API.get(
        apiName,
        `/session/${sessionName}`,
        initData
      );
      await commit("GET_SESSION", setSessionCandidateFullName(response));
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async getOpenedSession({ commit, dispatch }, sessionName) {
    try {
      const response = await API.get(
        apiUnAuthName,
        `/test/${sessionName}`,
        initData
      );
      await commit("GET_SESSION", setSessionCandidateFullName(response));
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async getSessions({ commit, dispatch }) {
    try {
      const response = await API.get(apiName, "/session", initData);
      commit(
        "GET_SESSIONS",
        response.map(session => setSessionCandidateFullName(session))
      );
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async recommendSession({ commit, dispatch }, { name, recommendation }) {
    try {
      const response = await API.post(apiName, `/session/${name}/recommend`, {
        ...initData,
        body: {
          recommendation: recommendation
        }
      });

      commit("DECIDE_SESSION", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** answer ***/
  async getAnswer({ commit, dispatch }, answerName) {
    try {
      const response = await API.get(
        apiName,
        `/answer/${answerName}`,
        initData
      );
      commit("GET_ANSWER", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async submitAnswer({ commit, dispatch }, payload) {
    try {
      const { name, ...data } = payload;
      const response = await API.post(apiUnAuthName, `/test/${name}`, {
        ...initData,
        body: data
      });
      commit("SUBMIT_ANSWER", payload);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async rateAnswer({ commit, dispatch }, payload) {
    try {
      const response = await API.post(apiName, `/answer/${payload.name}/rate`, {
        ...initData,
        body: payload
      });
      commit("RATE_ANSWER", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** authentication ***/
  async signIn({ commit, dispatch }, { email, password }) {
    try {
      await Auth.signIn(email, password);
      const user = await Auth.currentAuthenticatedUser();
      commit("SET_USER", user);
      return user;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async inviteUser({ dispatch }) {
    try {
      let part1 = "";
      let part2 = "";
      const numbers = "0123456789";
      const characters = "abcdefghijklmnopqrstuvwxyz";

      for (let i = 0; i < 3; i++) {
        part1 += numbers.charAt(Math.floor(Math.random() * numbers.length));
        part2 += characters.charAt(Math.floor(Math.random() * characters.length));
      }

      let array = (part1 + part2).split("");
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }

      const password = array.join("");
      // console.log(password);

      await Auth.signUp({
        username: "nuky99@naver.com",
        password: password,
        attributes: {
          email: "nuky99@naver.com",
          name: "Knowru",
          given_name: "John",
          family_name: "Doe",
          phone_number: "+821090690981"
        }
      });

      const user = await Auth.currentAuthenticatedUser();
      // console.log(user.attributes.email);
      // console.log(user.attributes.family_name);
      // console.log(user.attributes.given_name);
      const username = user.attributes.given_name + " " + user.attributes.family_name;
      const subject = "You are invited by " + username;
      const message = "You are invited by <strong>"+username+"</strong><br><a href='http://dev.ai-interview.com/en/auth/sign-in' target='_blank'>Click this link.</a> Please Sign In with this password<br><strong>" + password + "</strong>";

      const response = await API.post(apiName, "/email", {
        ...initData,
        body: {
          email: "nuky99@naver.com",
          subject: subject,
          message: message
        }
      });

      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async signUp(
    { dispatch },
    {
      email,
      password,
      organization,
      first_name,
      last_name,
      country_code,
      phone_number
    }
  ) {
    try {
      await API.post(apiUnAuthName, "/signup", {
        ...initData,
        body: {
          user_pool_id: environment.amplify.auth.userPoolId,
          organization: organization
        }
      });
      const response = await Auth.signUp({
        username: email,
        password,
        attributes: {
          email,
          name: organization,
          given_name: first_name,
          family_name: last_name,
          phone_number: country_code + phone_number
        }
      });
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async forgotPassword({ commit, dispatch }, { email }) {
    try {
      const response = Auth.forgotPassword(email);
      commit("FORGOT_PASSWORD", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async forgotPasswordSubmit(
    { commit, dispatch },
    { email, code, new_password }
  ) {
    try {
      const response = Auth.forgotPasswordSubmit(email, code, new_password);
      commit("FORGOT_PASSWORD_SUBMIT", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async confirmSignUp({ commit, dispatch }, { email, code }) {
    try {
      const response = Auth.confirmSignUp(email, code);
      commit("CONFIRM_SIGN_UP", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async resendSignUp({ commit, dispatch }, { email }) {
    try {
      const response = Auth.resendSignUp(email);
      commit("RESEND_SIGN_UP", response);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async signOut({ commit, dispatch }) {
    try {
      const response = Auth.signOut();
      commit("SET_USER", null);
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  /*** etc ***/
  async sendSms(
    { dispatch },
    { message = "Hello SMS!", phone_number = "+821082210886" }
  ) {
    try {
      const sns = new AWS.SNS({ apiVersion: "2010-03-31" });
      const params = {
        Message: message,
        MessageStructure: "message",
        PhoneNumber: phone_number
      };

      const response = await sns.publish(params).promise();
      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async sendEmail({ dispatch }, { message, subject, email }) {
    try {
      const response = await API.post(apiName, "/email", {
        ...initData,
        body: {
          email: email,
          subject: subject,
          message: message
        }
      });

      return response;
    } catch (e) {
      return handleError(e, dispatch);
    }
  },
  async testApi({ dispatch, commit }, payload) {
    try {
      // const response = await API.get(apiName, "/session", initData);
      // commit(
      //   "GET_SESSIONS",
      //   response.map(session => setSessionCandidateFullName(session))
      // );
      // return response;
      return {
        string: "string",
        number: 1,
        boolean: true,
        array: [1, "2", true]
      };
    } catch (e) {
      return handleError(e, dispatch);
    }
  }
};

export default actions;
