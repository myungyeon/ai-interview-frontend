import Vue from "vue";

const removeItemByName = (items, name) => {
  const index = items.findIndex(item => item.name === name);
  if (index > -1) items.splice(index, 1);
};

const mergeItemByName = (items, name, newItem) => {
  const item = items.find(item => item.name === name);
  for (const key of Object.keys(newItem)) {
    item[key] = newItem[key];
  }
};

const mutations = {
  // Video
  UPLOAD_VIDEO(state, { file, fileName }) {
    state.video = file;
    state.uploaded[fileName] = true;
  },
  // Question
  GET_QUESTIONS(state, questions) {
    state.questions = questions;
  },
  ADD_QUESTION(state, question) {
    state.questions.unshift(question);
  },
  EDIT_QUESTION(state, { name, question }) {
    mergeItemByName(state.questions, name, question);
  },
  DELETE_QUESTION(state, questionName) {
    removeItemByName(state.questions, questionName);
  },
  // Interview
  GET_INTERVIEWS(state, interviews) {
    state.interviews = interviews;
  },
  GET_INTERVIEW(state, interview) {
    state.interview = interview;
  },
  ADD_INTERVIEW(state, interview) {
    state.interviews.unshift(interview);
  },
  EDIT_INTERVIEW(state, { name, interview }) {
    mergeItemByName(state.interviews, name, interview);
  },
  DELETE_INTERVIEW(state, interviewName) {
    removeItemByName(state.interviews, interviewName);
  },
  // Candidate
  GET_CANDIDATES(state, candidates) {
    state.candidates = candidates;
  },
  GET_CANDIDATE(state, candidate) {
    state.candidate = candidate;
  },
  ADD_CANDIDATE(state, candidate) {
    state.candidates.unshift(candidate);
  },
  EDIT_CANDIDATE(state, { name, candidate }) {
    mergeItemByName(state.candidates, name, candidate);
  },
  DELETE_CANDIDATE(state, candidateName) {
    removeItemByName(state.candidates, candidateName);
  },
  // SESSION AND ANSWER
  GET_SESSION(state, session) {
    state.session = session;
  },
  GET_SESSIONS(state, sessions) {
    state.sessions = sessions;
  },
  DECIDE_SESSION(state, res) {
    state.session.decision = res.decision;
  },
  GET_ANSWER(state, answer) {
    state.answer = answer;
  },
  SUBMIT_ANSWER(state, answer) {
    const answers = state.session.answers;
    const answerIndex = answers.findIndex(a => a.name === answer.name);

    Vue.set(answers, answerIndex, {
      ...answers[answerIndex],
      ...answer
    });
  },
  RATE_ANSWER(state, payload) {
    state.session.average = payload.average;
  },
  SEND_INVITATIONS() {},
  OPEN_SNACKBAR(state, snackbar) {
    Vue.set(state, "snackbar", snackbar);
  },
  CLOSE_SNACKBAR(state) {
    state.snackbar.value = false;
  },
  SET_USER(state, user) {
    state.user = user;
  },
  CONFIRM_SET_USER(state, data) {
    console.log(data);
  },
  FORGOT_PASSWORD(state, data) {
    console.log(data);
  },
  FORGOT_PASSWORD_SUBMIT(state, data) {
    console.log(data);
  },
  SIGN_OUT(state, data) {
    console.log(data);
  }
};

export default mutations;
