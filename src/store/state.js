const state = {
  uploaded: {},
  video: null,
  snackbar: null,
  questions: [],
  interviews: [],
  candidates: [],
  sessions: [],
  session: null,
  interview: null,
  candidate: null,
  answer: null,
  user: null
};

export default state;
