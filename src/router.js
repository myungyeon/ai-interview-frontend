import Vue from "vue";
import App from "./App";
import Router from "vue-router";
import i18n from "./utils/i18n";
import store from "./store";
import { Auth } from "aws-amplify";

import RecruiterLayout from "./components/layout/RecruiterLayout";
import CandidateLayout from "./components/layout/CandidateLayout";
import AuthLayout from "./components/layout/AuthLayout";

const RecruiterInterview = () =>
  import("./containers/recruiter/RecruiterInterview");
const RecruiterQuestion = () =>
  import("./containers/recruiter/RecruiterQuestion");
const RecruiterCandidate = () =>
  import("./containers/recruiter/RecruiterCandidate");
const RecruiterSession = () =>
  import("./containers/recruiter/RecruiterSession");
const RecruiterTest = () => import("./containers/recruiter/RecruiterTest");
const CandidateSession = () =>
  import("./containers/candidate/CandidateSession");
const CandidateAnswer = () => import("./containers/candidate/CandidateAnswer");
const SignIn = () => import("./containers/auth/SignIn");
const SignUp = () => import("./containers/auth/SignUp");
const Forgot = () => import("./containers/auth/Forgot");
const Confirm = () => import("./containers/auth/Confirm");
const Home = () => import("./containers/Home");

Vue.use(Router);

const getDefaultLanguage = () => {
  const supportedLanguages = ["ko", "en", "cn"];
  const browserLanguage = window.navigator.language;

  const result = supportedLanguages.find(l =>
    browserLanguage.toLowerCase().includes(l)
  );

  return result ? result : "en";
};

const getLoggedInUser = async () => {
  try {
    const user = store.state.user || (await Auth.currentAuthenticatedUser());
    return user && user.signInUserSession ? user : null;
  } catch (e) {
    console.error(e);
    return null;
  }
};

const setLoggedInUserIfNotExistInState = user => {
  if (!store.user && user) {
    store.commit("SET_USER", user);
  }
};

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: `/${i18n.locale}`
    },
    {
      path: "/:lang",
      beforeEnter(to, from, next) {
        const lang = to.params.lang;

        if (!["en", "cn", "ko"].includes(lang))
          return next(getDefaultLanguage());

        if (i18n.locale !== lang) i18n.locale = lang;

        return next();
      },
      component: App,
      children: [
        {
          path: "",
          name: "Home",
          component: Home
        },
        {
          path: "auth",
          name: "Auth",
          component: AuthLayout,
          children: [
            {
              path: "sign-in",
              name: "SignIn",
              component: SignIn
            },
            {
              path: "sign-up",
              name: "SignUp",
              component: SignUp
            },
            {
              path: "forgot",
              name: "Forgot",
              component: Forgot
            },
            {
              path: "confirm",
              name: "Confirm",
              component: Confirm
            }
          ]
        },
        {
          path: "recruiter",
          component: RecruiterLayout,
          children: [
            {
              path: "interview/:name?",
              name: "RecruiterInterview",
              component: RecruiterInterview
            },
            {
              path: "question",
              name: "Questions",
              component: RecruiterQuestion
            },
            {
              path: "session/:name?",
              name: "RecruiterSession",
              component: RecruiterSession
            },
            {
              path: "candidate/:name?",
              name: "RecruiterCandidate",
              component: RecruiterCandidate
            },
            {
              path: "test",
              name: "RecruiterTest",
              component: RecruiterTest
            }
          ]
        },
        {
          path: "candidate",
          component: CandidateLayout,
          children: [
            {
              path: "session/:name",
              name: "CandidateSession",
              component: CandidateSession
            },
            {
              path: "answer/:name",
              name: "CandidateAnswer",
              component: CandidateAnswer
            }
          ]
        },
        {
          path: "**",
          component: Home
        }
      ]
    }
  ]
});

router.beforeResolve(async (to, from, next) => {
  const user = await getLoggedInUser();
  setLoggedInUserIfNotExistInState(user);

  if (to.path.includes("recruiter") && !user)
    return next({
      path: `/${i18n.locale}/auth/sign-in`,
      query: {
        redirectTo: to.fullPath
      }
    });

  if (to.path.includes("auth") && user)
    return next({
      path: `/${i18n.locale}/recruiter/session`
    });

  return next();
});

export default router;
