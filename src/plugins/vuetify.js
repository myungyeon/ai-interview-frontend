import Vue from "vue";
import Vuetify from "vuetify/lib";
// import "vuetify/src/stylus/app.styl";
import "./theme.styl";

Vue.use(Vuetify, {
  iconfont: "md"
});

import Confirm from "@/components/modal/Confirm";

function Install(Vue, options) {
  const property = (options && options.property) || "$confirm";
  function createDialogCmp(options) {
    return new Promise(resolve => {
      const cmp = new Vue(
        Object.assign(Confirm, {
          destroyed: c => {
            document.body.removeChild(cmp.$el);
            resolve(cmp.value);
          }
        })
      );
      Object.assign(cmp, Vue.prototype.$confirm.options || {}, options);
      document.body.appendChild(cmp.$mount().$el);
    });
  }

  function show(message, options = {}) {
    options.message = message;
    return createDialogCmp(options);
  }

  Vue.prototype[property] = show;
  Vue.prototype[property].options = options || {};
}

Vue.use(Install);
