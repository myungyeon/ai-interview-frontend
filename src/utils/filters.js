import Vue from "vue";

export const secondsToFriendlyTime = value => {
  if (value === undefined || value === null) {
    return null;
  }
  const m = 60;
  const h = 60 * 60;

  const hours = Math.floor(value / h);
  const minutes = Math.floor((value % h) / m);
  const seconds = value % m;

  let text = "";

  if (hours > 0) {
    text += `${hours} hour(s) `;
  }

  if (minutes > 0) {
    text += `${minutes} minute(s) `;
  }

  if (seconds > 0) {
    text += `${seconds} second(s)`;
  }

  return text;
};

export const secondsToDigitalTime = value => {
  if (value === undefined || value === null) {
    return null;
  }
  const m = 60;
  const h = 60 * 60;

  const hours = Math.floor(value / h);
  const minutes = Math.floor((value % h) / m);
  const seconds = value % m;

  let text = "";

  text += `${hours < 10 ? "0" + hours : hours}:`;
  text += `${minutes < 10 ? "0" + minutes : minutes}:`;
  text += `${seconds < 10 ? "0" + seconds : seconds}`;

  return text;
};

export const secondsToMinutes = value => {
  return value / 60;
};

export const sliceToISOYYYMMDD = value => {
  return value ? new Date(value).toLocaleString() : null;
};

export const properCase = (value, separator = "_") => {
  if (!value) return null;

  const segs = value.split(separator);

  return segs.map(seg => seg[0].toUpperCase() + seg.slice(1)).join(" ");
};

export const capitalize = value => {
  if (!value) return "";
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
};

Vue.filter("secondsToFriendlyTime", secondsToFriendlyTime);
Vue.filter("secondsToDigitalTime", secondsToDigitalTime);
Vue.filter("secondsToMinutes", secondsToMinutes);
Vue.filter("sliceToISOYYYMMDD", sliceToISOYYYMMDD);
Vue.filter("properCase", properCase);
Vue.filter("capitalize", capitalize);
