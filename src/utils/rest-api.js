import { Auth, Storage, API } from "aws-amplify";

/**
 * TODO: Action에서 API 작성 필요
 */

const apiName = "AI-Interview-Auth";
const apiUnAuthName = "AI-Interview-UnAuth";
const initData = {
  headers: {}
};

export class RestApi {
  /**
   * Get Question List
   * @returns {Promise<any>}
   */
  static getQuestions() {
    return API.get(apiName, "/question", initData);
  }
  /**
   * Add Question
   * @param { title, question, type, time_limit, }
   * @returns {Promise<any>}
   */
  static addQuestion(value) {
    return API.post(apiName, "/question", {
      ...initData,
      body: value
    });
  }
  /**
   * Edit Question
   * @param { name, title, question, type, time_limit }
   * @returns {Promise<any>}
   */
  static editQuestion({ name, ...value }) {
    return API.put(apiName, `/question/${name}`, {
      ...initData,
      body: value
    });
  }
  /**
   * Delete Question
   * @param name
   * @returns {Promise<any>}
   */
  static deleteQuestion(name) {
    return API.del(apiName, `/question/${name}`, initData);
  }
  /**
   * Get Interview List
   * @returns {Promise<any>}
   */
  static getInterviews() {
    return API.get(apiName, "/interview", initData);
  }
  /**
   * Get Interview
   * @param name
   * @returns {Promise<any>}
   */
  static getInterview(name) {
    return API.get(apiName, `/interview/${name}`, initData);
  }

  static addInterview(value) {
    return API.post(apiName, "/interview", {
      ...initData,
      body: value
    });
  }
  static editInterview({ name, ...value }) {
    return API.put(apiName, `/interview/${name}`, {
      ...initData,
      body: value
    });
  }
  static inviteToInterview({ name, ...value }) {
    return API.post(apiName, `/interview/${name}/invite`, {
      ...initData,
      body: value
    });
  }
  static deleteInterview(name) {
    return API.del(apiName, `/interview/${name}`, initData);
  }
  static getCandidates() {
    return API.get(apiName, "/candidate", initData);
  }
  static getCandidate(name) {
    return API.get(apiName, `/candidate/${name}`, initData);
  }
  static addCandidate(value) {
    return API.post(apiName, "/candidate", {
      ...initData,
      body: value
    });
  }
  static editCandidate({ name, ...value }) {
    return API.put(apiName, `/candidate/${name}`, {
      ...initData,
      body: value
    });
  }
  static deleteCandidate(name) {
    return API.del(apiName, `/candidate/${name}`, initData);
  }
}
