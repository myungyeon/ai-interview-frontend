export const FormModalMixin = {
  props: {
    value: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    value(v) {
      if (v === false) this.clear();
    }
  },
  methods: {
    close() {
      this.$emit("input", false);
    },
    validate() {
      return this.$refs.validator ? this.$refs.validator.validate() : false;
    },
    setValue({ name, ...value }) {
      this.name = name;

      for (const key of Object.keys(this.form.value)) {
        if (value[key] === null || value[key] === undefined) continue;
        this.form.value[key] = value[key];
      }
    },
    clear() {
      const data = this.initData();
      for (const key of Object.keys(data)) {
        this[key] = data[key];
      }

      if (this.$refs.validator) this.$refs.validator.initialize();
    }
  }
};
