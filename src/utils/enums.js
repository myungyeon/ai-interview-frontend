export const questionTypeEnums = {
  Essay: "short_essay",
  short_essay: "Essay",
  Coding: "coding",
  coding: "Coding",
  Video: "video",
  video: "Video"
};
